#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created: Tue Apr 26 10:13:16 2022

@author: evan
"""

from file_tree import FileTree, convert

# Keys to convert (present in both trees)
keys = ["t1", "rest", "task", "fmap1", "fmap2"]

src_data = "src_data"       # Path to source data dir
target_data = "target_data" # Path to target data dir
stree = "src.tree"          # Path to source tree file
ttree = "target.tree"       # Path to target tree file


src_read = FileTree.read(stree, src_data).update_glob(keys)
target_read = FileTree.read(ttree, target_data)

convert(src_read, target_read, keys, symlink=True)
