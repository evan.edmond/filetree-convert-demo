# Using file_tree to convert dataset file structures

While most imaging datasets are structured in some way, there are a wide variety of standards. 
Everyone might converge on using the BIDS format (https://bids-specification.readthedocs.io/en/stable/index.html) at some point, but in the meantime, conversion is common. 
While there are other dedicated BIDS converters available, I have found these small functions helpful in converting multiple datasets into a common space for analysis. 

## Prerequisites
It depends on the file_tree module - documentation here https://open.win.ox.ac.uk/pages/ndcn0236/file-tree/
You can install this by running

```
pip install file-tree file-tree-fsl
```

## Source dataset
I have created a sample dataset in the ./src_data directory that contains the following file structure, summarised in the src.tree file.

```
F3T_2022_{participant}
  images_002_mprax111withearsandnose1001.nii.gz (t1)
  images_003_ep2daxialresting.nii.gz (rest)
  images_006_30degTCTR3seczshim.nii.gz (task)
  images_004_grefieldmapping521Hzpx1001.nii.gz (fmap1)
  images_004_grefieldmapping521Hzpx2001.nii.gz (fmap2)
```

## Destination dataset
We want to convert the dataset to the following format, summarised in the dest.tree file. The keys in the parentheses () must be found in both source and destination filetrees. 
```
rawdata
  sub-{participant}
    anat
      sub-{participant}_T1w.nii.gz (t1)
    func
      sub-{participant}_task-rest_bold.nii.gz (rest)
      sub-{participant}_task-finger_bold.nii.gz (task)
    fmap
      sub-{participant}_fieldmap1.nii.gz (fmap1)
      sub-{participant}_fieldmap2.nii.gz (fmap2)
```

## Conversion
An example conversion is shown in the 'run_conversion.py' script. You can run this from the terminal with:
```
python3 run_conversion.py
```

## How it works
Complicated dataset conversion can be accomplished with code as simple as this!

```python
from file_tree import FileTree, convert

# Keys to convert (present in both trees)
keys = ["t1", "rest", "task", "fmap1", "fmap2"]

src_data = "src_data"       # Path to source data dir
target_data = "target_data" # Path to target data dir
stree = "src.tree"          # Path to source tree file
ttree = "target.tree"       # Path to target tree file


src_read = FileTree.read(stree, src_data).update_glob(keys)
target_read = FileTree.read(ttree, target_data)

convert(src_read, target_read, keys, symlink=True)
```
